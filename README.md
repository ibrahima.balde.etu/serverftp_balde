# Application ServerFTPTree

## Auteur:

    - Ibrahima BALDE

## Responsable:

    - Arthur d'Azemar

Date : 03 fevrier 2021

## Description du projet

Ce projet permet de mettre en œuvre une commande shell permettant d'afficher sur la sortie standard d'un terminal  l'arborescence d'un répertoire distant accessible via le protocole applicatif File Transfer Protocol (FTP). Le rendu de l'arborescence distante s'inspirera du formalisme utilisé la commande tree de Linux.

## Compilation

```bash
mvn package permet de compiler le code java
```

## Exécution

```bash
    java -jar <target>/FTPServer-1.3-SNAPSHOT.jar <nom du serveur>
```

## Architecture du projet

### Diagramme de classe

![diagramme de classe](diagramme_uml.PNG)

### Gestion d'erreurs

Les erreurs sont gerées par un bloc de try catch. Si on rencontre une erreur de manière generale, l'exception IOException est declanchée. Et dans le cas precis où on n'arrive pas à se connecter au serveur, l'exception FailedAuthentification est declanchée. Nous pouvons voir un exemple ci dessous.

```java
            String host = args[0];
            ServerConnect.host = host;
            try {
                Socket socketCommand = new Socket(host, ServerConnect.port);
                ClientFtp connection = new ClientFtp(socketCommand);
                Client client = new Client("anonymous", "anonymous");
                connection.connect(client);

                getTree(connection, " ");
            } catch (IOException | FailedAuthentification e) {
                e.printStackTrace();
            }
            .......

```

## Code Samples

Cette fonction permet de faire un parcours en profondeur de tous les dossiers d'un serveur ftp.

```java

public static void getTree(ClientFtp connection , String lineUp) throws IOException {

        // connection en mode passif
        Pasv commandPassive = new Pasv();
        connection.sendCommand(commandPassive);
        String passive = connection.readMessageServer();

        // recuperation de l'adresse ip
        String ip = commandPassive.getIp(passive);
        // recuperation du port
        int port = commandPassive.getPort(passive);

        Socket socketServer = new Socket( ip, port );
        // Envoie de la commande LIST
        Command ls = new List();
        connection.sendCommand(ls);

        InputStream in = socketServer.getInputStream();
        InputStreamReader in2 = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(in2);
        String content;

        while ( (content = reader.readLine() ) != null){
            System.out.println("|" + lineUp +" " + getFolderName(content) );
            if (content.charAt(0) == 'd'){
                // Envoie de la commande cwd
                Command cd = new Cwd( getFolderName(content));
                connection.sendCommand(cd);
                String lineUp2 = lineUp + "___";
                connection.readMessageServer();
                connection.readMessageServer();
                connection.readMessageServer();

                // recursivite
                getTree(connection, lineUp2 );

                // Envoie de la commande cwd pour sortir
                Command cd2 = new Cwd("..");
                connection.sendCommand(cd);
                connection.readMessageServer();
            }

        }

    }
```

Cette fonction permet de connecter un clientFtp sur un serveur 

```java
public void connect( Client client ) throws IOException, FailedAuthentification {
        this.printer.println( "USER " + client.getUser() + "\r\n" );
        this.printer.println("PASS " + client.getPassword() + "\r\n");
        System.out.println(readMessageServer());
        System.out.println(readMessageServer());
        String succesConnection =  readMessageServer();

        if (!succesConnection.startsWith("230")) {
            throw new FailedAuthentification("Connection non reussi");
        }
        System.out.println(succesConnection);

    }
```

*** Note : ***  

Vous trouverez la demo dans le dossier docs.

Quelques tests ont été implémentés.
Quand je renomme mon projet en TreeFtp les tests du maven ne marchent pas.