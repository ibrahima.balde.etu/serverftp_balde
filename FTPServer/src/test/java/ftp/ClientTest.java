package ftp;

import static org.junit.Assert.*;

public class ClientTest {

    private Client client1;
    private Client client2;

    @org.junit.Before
    public void setUp() throws Exception {
         this.client1 = new Client("ibrahima", "balde");
         this.client2 = new Client("ubuntu", "root");
    }

    @org.junit.Test
    public void getUser() {
        assertEquals("ibrahima", this.client1.getUser() );
        assertNotEquals( "ibrahima", this.client2.getUser() );
    }

    @org.junit.Test
    public void setUser() {
        this.client1.setUser("serveur");
        assertEquals( "serveur", this.client1.getUser() );
    }

    @org.junit.Test
    public void getPassword() {
        assertEquals("balde", this.client1.getPassword() );
        assertNotEquals( "ibrahima", this.client2.getPassword() );
    }

    @org.junit.Test
    public void setPassword() {
        this.client1.setPassword("root");
        assertEquals( "root", this.client1.getPassword()  );
    }
}