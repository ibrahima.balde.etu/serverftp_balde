package commande;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CwdTest {

    private Cwd cmd;

    @Before
    public void setUp() throws Exception {
        this.cmd = new Cwd("ubuntu");
    }

    @Test
    public void command() {
        assertEquals( "CWD " + "ubuntu" + "\r\n" , this.cmd.command() );
    }
}