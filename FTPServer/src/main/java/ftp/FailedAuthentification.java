package ftp;

public class FailedAuthentification extends Throwable {
    public FailedAuthentification(String message) {
        super(message);
    }
}
