package ftp;

import commande.Command;
import commande.Cwd;
import commande.List;
import commande.Pasv;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ServerConnect {

    private static final int port = 21;
    private static String host ;

    public static void main(String[] args) throws IOException {
        if (args.length > 0){
            String host = args[0];
            ServerConnect.host = host;
            try {
                Socket socketCommand = new Socket(host, ServerConnect.port);
                ClientFtp connection = new ClientFtp(socketCommand);
                Client client = new Client("anonymous", "anonymous");
                connection.connect(client);

                getTree(connection, " ");
            } catch (IOException | FailedAuthentification e) {
                e.printStackTrace();
            }


        }

    }


    public static void getTree(ClientFtp connection , String lineUp) throws IOException {

        // connection en mode passif
        Pasv commandPassive = new Pasv();
        connection.sendCommand(commandPassive);
        String passive = connection.readMessageServer();

        // recuperation de l'adresse ip
        String ip = commandPassive.getIp(passive);
        // recuperation du port
        int port = commandPassive.getPort(passive);

        Socket socketServer = new Socket( ip, port );
        // Envoie de la commande LIST
        Command ls = new List();
        connection.sendCommand(ls);

        InputStream in = socketServer.getInputStream();
        InputStreamReader in2 = new InputStreamReader(in);
        BufferedReader reader = new BufferedReader(in2);
        String content;

        while ( (content = reader.readLine() ) != null){
            System.out.println("|" + lineUp +" " + getFolderName(content) );
            if (content.charAt(0) == 'd'){
                // Envoie de la commande cwd
                Command cd = new Cwd( getFolderName(content));
                connection.sendCommand(cd);
                String lineUp2 = lineUp + "___";
                connection.readMessageServer();
                connection.readMessageServer();
                connection.readMessageServer();

                // recursivite
                getTree(connection, lineUp2 );

                // Envoie de la commande cwd pour sortir
                Command cd2 = new Cwd("..");
                connection.sendCommand(cd);
                connection.readMessageServer();
            }

        }

    }

    public static String getFolderName( String content ){
        StringBuilder input = new StringBuilder();
        input.append(content);
        input.reverse();
        String []s = input.toString().split(" ");
        input = new StringBuilder();
        input.append(s[0]);
        input.reverse();
        String nameDirectory = input.toString();
        return nameDirectory;
    }
}

