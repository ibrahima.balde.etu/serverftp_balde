package ftp;

import commande.Command;

import java.io.*;
import java.net.Socket;
import java.util.stream.Stream;

public class ClientFtp {

    private int port;
    private String server;
    private BufferedReader reader;
    private PrintWriter printer;
    private Socket socket;

    public ClientFtp( Socket socket) throws IOException {
        //Socket socket = new Socket(server, port);
        this.socket = socket;

        initReadAndWrite(socket);

    }

    public void initReadAndWrite( Socket socket ) throws IOException {
        InputStreamReader in = new InputStreamReader(socket.getInputStream());
        OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());

        this.reader = new BufferedReader(in);
        this.printer = new PrintWriter( out, true );
    }

    public void connect( Client client ) throws IOException, FailedAuthentification {
        this.printer.println( "USER " + client.getUser() + "\r\n" );
        this.printer.println("PASS " + client.getPassword() + "\r\n");
        System.out.println(readMessageServer());
        System.out.println(readMessageServer());
        String succesConnection =  readMessageServer();

        if (!succesConnection.startsWith("230")) {
            throw new FailedAuthentification("Connection non reussi");
        }
        System.out.println(succesConnection);

    }

    public String readMessageServer() throws IOException {
        return this.reader.readLine();
    }


    public void sendCommand(Command command){

        this.printer.println(command.command());
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }

    public PrintWriter getPrinter() {
        return printer;
    }

    public void setPrinter(PrintWriter printer) {
        this.printer = printer;
    }
}

