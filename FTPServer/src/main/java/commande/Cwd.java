package commande;

public class Cwd  implements Command{
    private String content;

    public Cwd(String content) {
        this.content = content;
    }

    @Override
    public String command( ) {

        return "CWD " + this.content + "\r\n";
    }
}
