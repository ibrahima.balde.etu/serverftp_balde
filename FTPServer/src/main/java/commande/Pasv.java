package commande;

public class Pasv implements Command {

    @Override
    public String command() {
        return "PASV"+"\r\n";
    }

    public String getIp(String passiveChaine) {
        //System.out.println(passiveChaine+ "   on est ok");
        String[] chaine = passiveChaine.substring(passiveChaine.indexOf("(")).split(",");
        //System.out.println(chaine[0].substring(1) + "  chaine0");
        //System.out.println(chaine[1]+"  chaine 1");
        //System.out.println(chaine[2]+"  chaine 2");
        //System.out.println(chaine[3]+"  chaine 3");
        return chaine[0].substring(1)+"."+chaine[1]+"."+chaine[2]+"."+chaine[3];

    }

    public int getPort(String passiveChaine) {
        String[] chaine = passiveChaine.substring(passiveChaine.indexOf("(")).split(",");
        return Integer.parseInt(chaine[4])*256 + Integer.parseInt(chaine[5].substring(0,chaine[5].length()-2));

    }
}
